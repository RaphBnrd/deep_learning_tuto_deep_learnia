# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation as animation

# T = 1 # sec
# dt = 0.02 # sec
# t_tot = 5 # sec

# fig = plt.figure()
# ax = fig.add_subplot(111, aspect='equal', autoscale_on=False,
#                      xlim=(-5, 5), ylim=(-5, 5))
# ax.grid()

# dot, = ax.plot([], [], 'o')
# line, = ax.plot([], [], '-') 
# time_text = ax.text(0.02, 0.95, '', transform=ax.transAxes)

# def init():
#     """initialize animation"""
#     dot.set_data([], [])
#     line.set_data([], [])
#     time_text.set_text('')
#     return dot, line, time_text

# def animate(i):
#     """perform animation step"""   
#     idxs = np.arange(0, i)
#     x_dot = np.cos(2*np.pi/T * i*dt) * i*dt
#     y_dot = np.sin(2*np.pi/T * i*dt) * i*dt
#     x_line = np.cos(2*np.pi/T * idxs*dt) * idxs*dt
#     y_line = np.sin(2*np.pi/T * idxs*dt) * idxs*dt
#     dot.set_data(x_dot, y_dot)
#     line.set_data(x_line, y_line)
#     time_text.set_text('time = ' + str(round(i*dt, 2)) + ' s')
#     return dot, line, time_text


# ani = animation.FuncAnimation(fig, animate, frames=int(t_tot/dt),
#                               interval=dt*1000, blit=True, init_func=init)

# # Writer = animation.writers['ffmpeg']
# # writer = Writer(fps=int(1/dt), bitrate=3200)
# # ani.save('animation_spirale.mp4', writer=writer)

# plt.show()









####### WITH LEGEND #######

from matplotlib import pyplot as plt 
from matplotlib import animation

fig = plt.figure()

ax = plt.axes(xlim=(0, 2), ylim=(0, 100))
# plt.title('Evolution')

N = 3
line1, = ax.plot([], [], label = 'line '+str(1)) 
line2, = ax.plot([], [], label = 'line '+str(2)) 
line3, = ax.plot([], [], label = 'line '+str(3))
lines = [line1, line2, line3]

def init():    
    for line in lines:
        line.set_data([], [])
    return lines

def animate(i):
    for j,line in enumerate(lines):
        line.set_data([0, 2], [10*j,i])
    return lines

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=100, interval=20, blit=True)

plt.legend()
ax.set_title('Evolution')
plt.show()