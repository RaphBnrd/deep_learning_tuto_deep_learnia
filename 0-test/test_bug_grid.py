import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import colors as c





x1_range = np.arange(-110, -110+50)
x2_range = np.arange(60, 60+50)

# ___ plot animation _____________________________________________________
test_grid = []
for i in range(80):
    N_1a = int(i/80*x1_range.size)
    N_1b = x1_range.size - int(i/80*x1_range.size)
    grid = np.column_stack((np.zeros((x2_range.size, N_1a)), np.ones((x2_range.size, N_1b))))
    test_grid.append(grid)

print(test_grid[10][:10, :10])
print(test_grid[50][:10, :10])

# plt.figure()
# plt.pcolormesh(x1_range, x2_range, test_grid[0], shading='nearest', cmap='summer')
# plt.figure()
# plt.pcolormesh(x1_range, x2_range, test_grid[20], shading='nearest', cmap='summer')
# plt.figure()
# plt.pcolormesh(x1_range, x2_range, test_grid[40], shading='nearest', cmap='summer')
# plt.figure()
# plt.pcolormesh(x1_range, x2_range, test_grid[60], shading='nearest', cmap='summer')
# plt.show()


fig = plt.figure(figsize=(6, 4))
ax3 = fig.add_subplot(1, 1, 1, aspect='equal', autoscale_on=False,
                    xlim=(min(x1_range), max(x1_range)), ylim=(min(x2_range), max(x2_range)))

ims = []
for i in range(80):
    im = ax3.pcolormesh(x1_range, x2_range, test_grid[i], shading='nearest',
                cmap=c.ListedColormap(['red', 'blue']))
    ims.append([im])

ani = animation.ArtistAnimation(fig, ims)
plt.show()