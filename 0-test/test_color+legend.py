import matplotlib.pyplot as plt
from sklearn.datasets import make_circles
import matplotlib.patches as mpatches
import numpy as np
from matplotlib import colors as c


X, y = make_circles(n_samples=400, noise=0.1, factor=0.3, random_state=0)
X = X.T
y = y.reshape((1, y.shape[0]))

X_train = X[:, :360]
y_train = y[:, :360]
X_test = X[:, 360:]
y_test = y[:, 360:]

x_grid, y_grid = np.array([-1, 0, 1, 2]), np.array([-1, 0, 1, 2])
pred = np.array([[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 1, 1], [0, 0, 0, 1]])
plt.pcolormesh(x_grid, y_grid, pred, shading='nearest', 
            #    color=['#faba08','#2dc43e'],
               cmap=c.ListedColormap(['#faba08','#2dc43e']),
               alpha=0.2,
               )

colors_train = np.zeros(y_train.shape[1], dtype=object)
colors_train[:] = '#faba08' # orange
colors_train[y_train.flatten()==1] = '#2dc43e' # green
colors_test = np.zeros(y_test.shape[1], dtype=object)
colors_test[:] = '#faf732' # yellow
colors_test[y_test.flatten()==1] = '#1df8b6' # blue-green
plt.scatter(X_train[0, :], X_train[1, :], c=colors_train)
plt.scatter(X_test[0, :], X_test[1, :], c=colors_test)

pop_train0 = mpatches.Patch(color='#faba08', label='train')
pop_train1 = mpatches.Patch(color='#2dc43e', label='train')
pop_test0 = mpatches.Patch(color='#faf732', label='test')
pop_test1 = mpatches.Patch(color='#1df8b6', label='test')

plt.legend(handles=[pop_train0, pop_train1, pop_test0, pop_test1])

plt.show()
