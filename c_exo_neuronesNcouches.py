import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_circles
from sklearn.metrics import accuracy_score, log_loss
from tqdm import tqdm

from utilities import *



def initialisation(dimensions):
    """dimensions = (n0, n1, ..., nN)"""
    parametres = {}
    N = len(dimensions)-1  # Nombre de couches
    for k in range(1, N+1):
        parametres['W'+str(k)] = np.random.randn(dimensions[k], dimensions[k-1])
        parametres['b'+str(k)] = np.zeros((dimensions[k], 1))

    return parametres


def forward_propagation(X, parametres):

    activations = {}

    Z1 = parametres['W1'].dot(X) + parametres['b1']
    activations['A1'] = 1 / (1 + np.exp(-Z1))

    N = len(parametres)//2
    for k in range(2, N+1):
        Zk = parametres['W'+str(k)].dot(activations['A'+str(k-1)]) + parametres['b'+str(k)]
        activations['A'+str(k)] = 1 / (1 + np.exp(-Zk))

    return activations



def back_propagation(X, y, parametres, activations):

    m = y.shape[1]
    N = len(parametres)//2
    dZ = {}
    gradients = {}

    
    dZ[str(N)] = activations['A'+str(N)] - y

    if N >= 2:
        gradients['dW'+str(N)] = 1 / m * dZ[str(N)].dot(activations['A'+str(N-1)].T)
        gradients['db'+str(N)] = 1 / m * np.sum(dZ[str(N)], axis=1, keepdims = True)

        for k in range(N-1, 1, -1):
            dZ[str(k)] = np.dot(parametres['W'+str(k+1)].T, dZ[str(k+1)]) * activations['A'+str(k)] * (1 - activations['A'+str(k)])
            gradients['dW'+str(k)] = 1 / m * dZ[str(k)].dot(activations['A'+str(k-1)].T)
            gradients['db'+str(k)] = 1 / m * np.sum(dZ[str(k)], axis=1, keepdims = True)

        dZ['1'] = np.dot(parametres['W2'].T, dZ['2']) * activations['A1'] * (1 - activations['A1'])
    
    gradients['dW1'] = 1 / m * dZ['1'].dot(X.T)
    gradients['db1'] = 1 / m * np.sum(dZ['1'], axis=1, keepdims = True)
    
    return gradients


def update(gradients, parametres, learning_rate):

    N = len(parametres)//2

    for k in range(1, N+1):
        parametres['W'+str(k)] = parametres['W'+str(k)] - learning_rate * gradients['dW'+str(k)]
        parametres['b'+str(k)] = parametres['b'+str(k)] - learning_rate * gradients['db'+str(k)]

    return parametres

def predict(X, parametres):
    N = len(parametres)//2
    activations = forward_propagation(X, parametres)
    return activations['A'+str(N)] >= 0.5


def neural_network(X_train, y_train, dimensions_internes=(), X_test=None, y_test=None, learning_rate = 0.1, n_iter = 1000):
    # initialisation parametres
    n0 = X_train.shape[0]
    nN = y_train.shape[0]
    dimensions = (n0, ) + dimensions_internes + (nN, )
    N = len(dimensions) - 1
    np.random.seed(0)
    parametres = initialisation(dimensions)

    train_loss, train_acc = [], []
    if (X_test is not None) and (y_test is not None):
        print('test')
        test_loss, test_acc = [], []
    history = []

    # gradient descent
    for i in tqdm(range(n_iter)):
        activations_train = forward_propagation(X_train, parametres)
        # Plot courbe d'apprentissage
        if i%10 == 0:
            train_loss.append(log_loss(y_train.flatten(), activations_train['A'+str(N)].flatten()))
            y_pred_train = predict(X_train, parametres)
            train_acc.append(accuracy_score(y_train.flatten(), y_pred_train.flatten()))

            if (X_test is not None) and (y_test is not None):
                activations_test = forward_propagation(X_test, parametres)
                test_loss.append(log_loss(y_test.flatten(), activations_test['A'+str(N)].flatten()))
                y_pred_test = predict(X_test, parametres)
                test_acc.append(accuracy_score(y_test.flatten(), y_pred_test.flatten()))
        
                history.append([parametres.copy(), train_loss[i//10], train_acc[i//10], test_loss[i//10], test_acc[i//10], i])
            else:
                history.append([parametres.copy(), train_loss[i//10], train_acc[i//10], i])

        # mise a jour
        gradients = back_propagation(X_train, y_train, parametres, activations_train)
        parametres = update(gradients, parametres, learning_rate)


    plt.figure(figsize=(12, 4))
    plt.subplot(1, 2, 1)
    plt.plot(train_loss, label='train loss')
    if (X_test is not None) and (y_test is not None):
        plt.plot(test_loss, label='test loss')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(train_acc, label='train acc')
    if (X_test is not None) and (y_test is not None):
        plt.plot(test_acc, label='test acc')
    plt.legend()
    plt.show()

    return parametres, history






# ### TEST THE FUNCTIONS ###
# # data
# X, y = make_circles(n_samples=100, noise=0.1, factor=0.3, random_state=0)
# X = X.T
# y = y.reshape((1, y.shape[0]))
# print('\ndimensions de X:', X.shape)
# print('dimensions de y:', y.shape)
# # init
# dimensions = (2, 3, 3, 4, 2, 1)
# print('\nNombre de neurones par couche (couche 0 = entrée):', dimensions)
# parametres = initialisation(dimensions)
# print('\nTaille de W3:', parametres['W3'].shape)
# print('Taille de b3', parametres['b3'].shape)
# for key, val in parametres.items():
#     print(key, val.shape)
# # forward propagation
# activations = forward_propagation(X, parametres)
# print('\nTaille de A3:', activations['A3'].shape)
# # back propagation
# gradients = back_propagation(X, y, parametres, activations)
# print('\nTaille de dW3:', gradients['dW3'].shape)
# print('Taille de db3:', gradients['db3'].shape)
# # update (descente de gradient)
# learning_rate = 0.01
# print('\nW3_(4,3):', parametres['W3'][4-1, 3-1])
# print('dW3_(4,3):', gradients['dW3'][4-1, 3-1])
# parametres = update(gradients, parametres, learning_rate)
# print('W3_(4,3):', parametres['W3'][4-1, 3-1])
# print('Taille de W3:', parametres['W3'].shape)
# print('Taille de b3:', parametres['b3'].shape)
# # prediction
# y_pred = predict(X, parametres)
# print('\nTaille de y_pred:', y_pred.shape)
# print('\nPrédiction du 17e élément:', y_pred.flatten()[16])
# print('Vraie valeur du 17e élément:', bool(y.flatten()[16]))
# print('Accuracy sur la prédiction', accuracy_score(y.flatten(), y_pred.flatten()))

# # neural network
# print('\n\nNeural network')
# parametres, history = neural_network(X, y, dimensions_internes=(3, 2, 4), 
#                             X_test=None, y_test=None,
#                             learning_rate = 0.1, n_iter = 1000)
# print(parametres.keys())


# ##### TEST DATA 1 #####
# # réseau de neurones
# X, y = make_circles(n_samples=100, noise=0.1, factor=0.3, random_state=0)
# X = X.T
# y = y.reshape((1, y.shape[0]))
# print('dimensions de X:', X.shape)
# print('dimensions de y:', y.shape)

# parametres, history = neural_network(X, y, dimensions_internes=(2, 2, 2), 
#                             X_test=None, y_test=None,
#                             learning_rate = 0.1, n_iter = 1000)





# ##### TEST DATA CATS AND DOGS #####
# X_train, y_train, X_test, y_test = load_data()

# X_train, X_test = X_train.T, X_test.T
# X_train = X_train.reshape(-1, X_train.shape[-1]) / X_train.max()
# X_test = X_test.reshape(-1, X_test.shape[-1]) / X_test.max()

# y_train, y_test = y_train.T, y_test.T

# m_train, m_test = 300, 80
# X_train, X_test = X_train[:, :m_train], X_test[:, :m_test]
# y_train, y_test = y_train[:, :m_train], y_test[:, :m_test]

# # print(X_train.shape)
# # print(X_test.shape)
# # print(y_train.shape)
# # print(y_test.shape)

# parametres, history = neural_network(X_train, y_train, dimensions_internes=(2, 2, 2), 
#                             X_test=X_test, y_test=y_test,
#                             learning_rate = 0.1, n_iter = 1000)
 





#### TEST OF THE CORRECTION
X, y = make_circles(n_samples=100, noise=0.1, factor=0.3, random_state=0)
X = X.T
y = y.reshape((1, y.shape[0]))

print('dimensions de X:', X.shape)
print('dimensions de y:', y.shape)

plt.scatter(X[0, :], X[1, :], c=y, cmap='summer')
plt.show()

neural_network(X, y, dimensions_internes = (16, 16, 16), learning_rate = 0.1, n_iter = 3000)

