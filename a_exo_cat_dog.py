from utilities import *
import matplotlib.pyplot as plt
from a_premier_neurone import initialisation, model, gradients, update
from sklearn import metrics

X_train, y_train, X_test, y_test = load_data()

# ### ___ Explore data ___ ###
# print(X_train.shape)
# print(y_train.shape)
# print(np.unique(y_train, return_counts=True))
# # 500 data of cats, 500 of dogs

# print(X_test.shape)
# print(y_test.shape)
# print(np.unique(y_test, return_counts=True))
# # 100 data of cats, 100 of dogs

# plt.figure(figsize=(16, 8))
# for i in range(1, 10):
#     plt.subplot(4, 5, i)
#     plt.imshow(X_train[i], cmap='gray')
#     plt.title(y_train[i])
#     plt.tight_layout()
# plt.show()




### ___ Functions ___ ###
def predict(X, W, b):
    A = model(X, W, b)
    return A >= 0.5
def artificial_neuron(X_train, y_train, X_test, y_test, learning_rate=0.1, n_iter=100):
    W, b = initialisation(X_train)
    Loss_train, Loss_test = [], []
    for i in range(n_iter):
        if i%1000 == 0:
            print('itération',i)
        A_train = model(X_train, W, b)
        Loss_train.append(metrics.log_loss(y_train, A_train))
        A_test = model(X_test, W, b)
        Loss_test.append(metrics.log_loss(y_test, A_test))
        dW, db = gradients(A_train, X_train, y_train)
        W, b = update(dW, db, W, b, learning_rate)
    y_train_pred = predict(X_train, W, b)
    print("Accuracy train", metrics.accuracy_score(y_train, y_train_pred))
    y_test_pred = predict(X_test, W, b)
    print("Accuracy test", metrics.accuracy_score(y_test, y_test_pred))
    return (W, b, Loss_train, Loss_test)



### ___ Solve problem ___ ###
X_train = X_train / 255
# X_train = np.reshape(X_train, (1000, 64*64))
X_train = np.reshape(X_train, (1000, -1))
# X_test = np.reshape(X_test/255, (200, 64*64))
X_test = np.reshape(X_test/255, (200, -1))
# print(X_train.shape, X_train[0].shape)
W, b, Loss_train, Loss_test = artificial_neuron(X_train, y_train, X_test, y_test, learning_rate=0.01, n_iter=50000)

plt.plot(Loss_train, label='train')
plt.plot(Loss_test, label='test')
plt.legend()
plt.show()


