import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as mpatches
from matplotlib import colors as c
from sklearn.datasets import make_circles
from sklearn.metrics import accuracy_score, log_loss
from tqdm import tqdm

from utilities import *



def initialisation(dimensions):
    """dimensions = (n0, n1, ..., nN)"""
    parametres = {}
    N = len(dimensions)-1  # Nombre de couches
    for k in range(1, N+1):
        parametres['W'+str(k)] = np.random.randn(dimensions[k], dimensions[k-1])
        parametres['b'+str(k)] = np.zeros((dimensions[k], 1))

    return parametres


def forward_propagation(X, parametres):

    activations = {}

    Z1 = parametres['W1'].dot(X) + parametres['b1']
    activations['A1'] = 1 / (1 + np.exp(-Z1))

    N = len(parametres)//2
    for k in range(2, N+1):
        Zk = parametres['W'+str(k)].dot(activations['A'+str(k-1)]) + parametres['b'+str(k)]
        activations['A'+str(k)] = 1 / (1 + np.exp(-Zk))

    return activations



def back_propagation(X, y, parametres, activations):

    m = y.shape[1]
    N = len(parametres)//2
    dZ = {}
    gradients = {}

    
    dZ[str(N)] = activations['A'+str(N)] - y

    if N >= 2:
        gradients['dW'+str(N)] = 1 / m * dZ[str(N)].dot(activations['A'+str(N-1)].T)
        gradients['db'+str(N)] = 1 / m * np.sum(dZ[str(N)], axis=1, keepdims = True)

        for k in range(N-1, 1, -1):
            dZ[str(k)] = np.dot(parametres['W'+str(k+1)].T, dZ[str(k+1)]) * activations['A'+str(k)] * (1 - activations['A'+str(k)])
            gradients['dW'+str(k)] = 1 / m * dZ[str(k)].dot(activations['A'+str(k-1)].T)
            gradients['db'+str(k)] = 1 / m * np.sum(dZ[str(k)], axis=1, keepdims = True)

        dZ['1'] = np.dot(parametres['W2'].T, dZ['2']) * activations['A1'] * (1 - activations['A1'])
    
    gradients['dW1'] = 1 / m * dZ['1'].dot(X.T)
    gradients['db1'] = 1 / m * np.sum(dZ['1'], axis=1, keepdims = True)
    
    return gradients


def update(gradients, parametres, learning_rate):

    N = len(parametres)//2

    for k in range(1, N+1):
        parametres['W'+str(k)] = parametres['W'+str(k)] - learning_rate * gradients['dW'+str(k)]
        parametres['b'+str(k)] = parametres['b'+str(k)] - learning_rate * gradients['db'+str(k)]

    return parametres

def predict(X, parametres):
    N = len(parametres)//2
    activations = forward_propagation(X, parametres)
    return activations['A'+str(N)] >= 0.5




def neural_network(X_train, y_train, dimensions_internes=(), 
                   X_test=None, y_test=None, learning_rate = 0.1, n_iter = 1000, 
                   step_store_data=10, name_export_anim=''):
    """Il faut que x_train n'aie que 2 paramètres"""
    # ___ initialisation of data for prediction zones ________________________
    x_1_min, x_1_max = X_train[0, :].min(), X_train[0, :].max()
    x_2_min, x_2_max = X_train[1, :].min(), X_train[1, :].max()
    nbr_box = 50
    x1_range = np.linspace(x_1_min-(x_1_max-x_1_min)*0.25, x_1_max+(x_1_max-x_1_min)*0.1, nbr_box)
    x2_range = np.linspace(x_2_min-(x_2_max-x_2_min)*0.1, x_2_max+(x_2_max-x_2_min)*0.25, nbr_box)
    grid_pred = np.zeros((x2_range.size, x1_range.size))
    # grid_pred is an array 2D : containing pred at (x1, x2)
    # ___ initialisation parametres __________________________________________
    n0 = X_train.shape[0]
    nN = y_train.shape[0]
    dimensions = (n0, ) + dimensions_internes + (nN, )
    N = len(dimensions) - 1
    np.random.seed(0)
    parametres = initialisation(dimensions)
    # ___ initialisation des outputs _________________________________________
    train_loss, train_acc = [], []
    test_loss, test_acc = [], []
    all_grid_pred = []
    history_params = []

    # ___ gradient descent ___________________________________________________
    for i in tqdm(range(n_iter)):
        activations_train = forward_propagation(X_train, parametres)
        # ___ mise a jour ___
        gradients = back_propagation(X_train, y_train, parametres, activations_train)
        parametres = update(gradients, parametres, learning_rate)

        if i%step_store_data == 0:
            # ___ Save params ___
            history_params.append(parametres.copy())
            # ___ Prediction on grid ___
            for k in range(x2_range.size):
                xk_grid = np.array([x1_range,[x2_range[k]]*nbr_box])
                grid_pred[k, :] = predict(xk_grid, parametres).flatten()
            all_grid_pred.append(grid_pred.copy())
            # ___ Add learning curves ___
            # on train
            train_loss.append(log_loss(y_train.flatten(), activations_train['A'+str(N)].flatten()))
            y_pred_train = predict(X_train, parametres)
            train_acc.append(accuracy_score(y_train.flatten(), y_pred_train.flatten()))
            # on test
            if (X_test is not None) and (y_test is not None):
                activations_test = forward_propagation(X_test, parametres)
                test_loss.append(log_loss(y_test.flatten(), activations_test['A'+str(N)].flatten()))
                y_pred_test = predict(X_test, parametres)
                test_acc.append(accuracy_score(y_test.flatten(), y_pred_test.flatten()))


    anim = plot_evolution_network(train_loss, train_acc, X_train, y_train, 
                        x1_range, x2_range, all_grid_pred,
                        dimensions_internes, learning_rate, n_iter,
                        step_store_data=step_store_data,
                        X_test=X_test, y_test=y_test,
                        test_loss=test_loss, test_acc=test_acc,
                        name_export_anim=name_export_anim)
    plt.show()


    return train_loss, train_acc, test_loss, test_acc, \
           history_params, x1_range, x2_range, all_grid_pred



def plot_evolution_network(train_loss, train_acc, X_train, y_train, 
                           x1_range, x2_range, all_grid_pred,
                           dimensions_internes, learning_rate, n_iter,
                           step_store_data=10,
                           X_test=None, y_test=None,
                           test_loss=[], test_acc=[], name_export_anim=''):
    
    colors_plot = {'dark_blue': '#4c71eb',
                   'bright_blue': '#4ce8eb',
                   'orange': '#faba08',
                   'yellow': '#faf732',
                   'dark_green': '#2dc43e',
                   'bright_green': '#1df8b6'}


    fig = plt.figure(figsize=(12, 4))

    # ___ Loss plot __________________________________________________________
    if len(test_acc) > 0:
        ax1_miny = min(min(train_loss), min(test_loss))
        ax1_maxy = max(max(train_loss), max(test_loss))
    else:
        ax1_miny = min(train_loss)
        ax1_maxy = max(train_loss)
    ax1 = fig.add_subplot(1, 3, 1, xlim=(0, n_iter), ylim=(ax1_miny, ax1_maxy))
    plot_train_loss, = ax1.plot([], [], c=colors_plot['dark_blue'], label='train')
    if len(test_loss) > 0:
        plot_test_loss, = ax1.plot([], [], c=colors_plot['bright_blue'], label='test')

    # ___ Accuracy plot ______________________________________________________
    if len(test_acc) > 0:
        ax2_miny = min(min(train_acc), min(test_acc))
        ax2_maxy = max(max(train_acc), max(test_acc))
    else:
        ax2_miny = min(train_acc)
        ax2_maxy = max(train_acc)
    ax2 = fig.add_subplot(1, 3, 2, xlim=(0, n_iter), ylim=(ax2_miny, ax2_maxy))
    plot_train_acc, = ax2.plot([], [], c=colors_plot['dark_blue'], label='train')
    if len(test_acc) > 0:
        plot_test_acc, = ax2.plot([], [], c=colors_plot['bright_blue'], label='test')

    # ___ Prediction _________________________________________________________
    ax3 = fig.add_subplot(1, 3, 3, aspect='equal', autoscale_on=False,
                        xlim=(min(x1_range), max(x1_range)), ylim=(min(x2_range), max(x2_range)))
    # colors for the scatter plot
    # train data
    colors_train_dot = np.zeros(y_train.shape[1], dtype=object)
    colors_train_dot[:] = colors_plot['orange']
    colors_train_dot[y_train.flatten()==1] = colors_plot['dark_green']
    ax3.scatter(X_train[0, :], X_train[1, :], c=colors_train_dot, s=6)
    pop_train0 = mpatches.Patch(color=colors_plot['orange'], label='train')
    pop_train1 = mpatches.Patch(color=colors_plot['dark_green'], label='train')
    # test data
    if (X_test is not None) and (y_test is not None):
        colors_test_dot = np.zeros(y_test.shape[1], dtype=object)
        colors_test_dot[:] = colors_plot['yellow']
        colors_test_dot[y_test.flatten()==1] = colors_plot['bright_green']
        ax3.scatter(X_test[0, :], X_test[1, :], c=colors_test_dot, s=6)
        pop_test0 = mpatches.Patch(color=colors_plot['yellow'], label='test')
        pop_test1 = mpatches.Patch(color=colors_plot['bright_green'], label='test')
    # colmesh
    col_mesh = ax3.pcolormesh(x1_range, x2_range, all_grid_pred[0], shading='nearest',
                              cmap=c.ListedColormap([colors_plot['orange'], colors_plot['dark_green']]),
                              alpha=0.4, vmin=0, vmax=1)
    time_text = ax3.text(0.02, 0.82, '', fontsize=9, transform=ax3.transAxes)
    
    # ___ Animation __________________________________________________________
    def init():
        col_mesh.set_array(all_grid_pred[0].ravel())
        time_text.set_text('')
        plot_train_loss.set_data([], [])
        plot_train_acc.set_data([], [])
        out = (col_mesh, time_text, plot_train_loss, plot_train_acc)
        if len(test_loss) > 0:
            plot_test_loss.set_data([], [])
            out += (plot_test_loss, )
        if len(test_acc) > 0:
            plot_test_acc.set_data([], [])
            out += (plot_test_acc, )
        return out
    def animate(i):
        col_mesh.set_array(all_grid_pred[i].ravel()) 
        time_text.set_text('Dimensions cachées: ' + str(dimensions_internes) + \
                        '\nLearning Rate: ' + str(learning_rate) + \
                        '\nItération: '+str(i*step_store_data))
        plot_train_loss.set_data(range(0, i*step_store_data+1, step_store_data), train_loss[:i+1])
        plot_train_acc.set_data(range(0, i*step_store_data+1, step_store_data), train_acc[:i+1])
        out = (col_mesh, time_text, plot_train_loss, plot_train_acc)
        if len(test_loss) > 0:
            plot_test_loss.set_data(range(0, i*step_store_data+1, step_store_data), test_loss[:i+1])
            out += (plot_test_loss, )
        if len(test_acc) > 0:
            plot_test_acc.set_data(range(0, i*step_store_data+1, step_store_data), test_acc[:i+1])
            out += (plot_test_acc, )
        return out

    ax1.legend(loc='upper right')
    ax1.set_title('Loss')
    ax2.legend(loc='upper left')
    ax2.set_title('Accuracy')
    ax3.legend(loc='upper right', handles=[pop_train0, pop_train1, pop_test0, pop_test1])
    ax3.set_title('Prediction')

    anim = animation.FuncAnimation(fig, animate, interval=5, frames=int(n_iter/step_store_data), init_func=init)

    if name_export_anim != '':
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=int(1000/30), bitrate=3200)
        anim.save(name_export_anim, writer=writer)


    return anim













# _____ INIT THE DATA ________________________________________________________
n_samples = 300
X1, y1 = make_circles(n_samples=n_samples, noise=0.1, factor=0.3, random_state=0)
X1 = X1.T + np.array([[-1], [0]])
y1 = y1.reshape((1, y1.shape[0]))
X2, y2 = make_circles(n_samples=n_samples, noise=0.1, factor=0.3, random_state=0)
X2 = X2.T + np.array([[0], [2]])
y2 = y2.reshape((1, y2.shape[0]))
X3, y3 = make_circles(n_samples=n_samples, noise=0.2, factor=0.2, random_state=0)
X3 = X3.T + np.array([[1.5], [0]])
y3 = y3.reshape((1, y3.shape[0]))

X = np.column_stack((X1, X2, X3))
y = np.column_stack((y1, y2, y3))


# _____ BUILD TRAIN AND TEST _________________________________________________
prop_train = 90  # 90%
choose_train = np.zeros(shape=y.shape[1], dtype=bool) # Array with N False
choose_train[:int(prop_train / 100 * choose_train.size)] = True  # Set the first k% of the elements to True
np.random.shuffle(choose_train)  # Shuffle the array

X_train = X[:, choose_train]
y_train = y[:, choose_train]
X_test = X[:, ~choose_train]
y_test = y[:, ~choose_train]

print('dimensions de X_train:', X_train.shape)
print('dimensions de y_train:', y_train.shape)
print('dimensions de X_test:', X_test.shape)
print('dimensions de y_test:', y_test.shape)

# plt.scatter(X[0, :], X[1, :], c=y, cmap='summer', s=6)
# plt.show()




# _____ LAUNCH NEURAL NETWORK ________________________________________________
dimensions_internes = (16, 16, 16)
n_iter = 5000
learning_rate = 0.1
step_store_data=10

outputs = neural_network(X_train, y_train, dimensions_internes=dimensions_internes, 
                   X_test=X_test, y_test=y_test, step_store_data=step_store_data,
                   learning_rate=learning_rate, n_iter=n_iter, 
                   name_export_anim='NeuralNetwork_'+str(dimensions_internes)+'_'+\
                       str(n_iter)+'_'+str(learning_rate)+'.mp4')

train_loss, train_acc, test_loss, test_acc = outputs[:4]
history_params, x1_range, x2_range, all_grid_pred = outputs[4:]


