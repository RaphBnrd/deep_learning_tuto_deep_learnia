import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_circles
from sklearn.metrics import accuracy_score, log_loss
from tqdm import tqdm

from a_premier_neurone import artificial_neuron
from utilities import *



def initialisation(n0, n1, n2):

    W1 = np.random.randn(n1, n0)
    b1 = np.zeros((n1, 1))
    W2 = np.random.randn(n2, n1)
    b2 = np.zeros((n2, 1))

    parametres = {
        'W1': W1,
        'b1': b1,
        'W2': W2,
        'b2': b2
    }

    return parametres

def forward_propagation(X, parametres):

    W1 = parametres['W1']
    b1 = parametres['b1']
    W2 = parametres['W2']
    b2 = parametres['b2']

    Z1 = W1.dot(X) + b1
    A1 = 1 / (1 + np.exp(-Z1))

    Z2 = W2.dot(A1) + b2
    A2 = 1 / (1 + np.exp(-Z2))

    activations = {
        'A1': A1,
        'A2': A2
    }

    return activations

def back_propagation(X, y, parametres, activations):

    A1 = activations['A1']
    A2 = activations['A2']
    W2 = parametres['W2']

    m = y.shape[1]

    dZ2 = A2 - y
    dW2 = 1 / m * dZ2.dot(A1.T)
    db2 = 1 / m * np.sum(dZ2, axis=1, keepdims = True)

    dZ1 = np.dot(W2.T, dZ2) * A1 * (1 - A1)
    dW1 = 1 / m * dZ1.dot(X.T)
    db1 = 1 / m * np.sum(dZ1, axis=1, keepdims = True)

    gradients = {
        'dW1' : dW1,
        'db1' : db1,
        'dW2' : dW2,
        'db2' : db2
    }
    
    return gradients

def update(gradients, parametres, learning_rate):

    W1 = parametres['W1']
    b1 = parametres['b1']
    W2 = parametres['W2']
    b2 = parametres['b2']

    dW1 = gradients['dW1']
    db1 = gradients['db1']
    dW2 = gradients['dW2']
    db2 = gradients['db2']

    W1 = W1 - learning_rate * dW1
    b1 = b1 - learning_rate * db1
    W2 = W2 - learning_rate * dW2
    b2 = b2 - learning_rate * db2

    parametres = {
        'W1': W1,
        'b1': b1,
        'W2': W2,
        'b2': b2
    }

    return parametres

def predict(X, parametres):
  activations = forward_propagation(X, parametres)
  A2 = activations['A2']
  return A2 >= 0.5

def neural_network(X, y, n1=32, learning_rate = 0.1, n_iter = 1000):

    # initialisation parametres
    n0 = X.shape[0]
    n2 = y.shape[0]
    np.random.seed(0)
    parametres = initialisation(n0, n1, n2)

    train_loss = []
    train_acc = []
    history = []

    # gradient descent
    for i in tqdm(range(n_iter)):
        activations = forward_propagation(X, parametres)
        A2 = activations['A2']

        # Plot courbe d'apprentissage
        train_loss.append(log_loss(y.flatten(), A2.flatten()))
        y_pred = predict(X, parametres)
        train_acc.append(accuracy_score(y.flatten(), y_pred.flatten()))
        
        history.append([parametres.copy(), train_loss, train_acc, i])

        # mise a jour
        gradients = back_propagation(X, y, parametres, activations)
        parametres = update(gradients, parametres, learning_rate)


    plt.figure(figsize=(12, 4))
    plt.subplot(1, 2, 1)
    plt.plot(train_loss, label='train loss')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(train_acc, label='train acc')
    plt.legend()
    plt.show()

    return parametres



def neural_network2(X_train, y_train, X_test, y_test, n1=32, learning_rate = 0.1, n_iter = 1000):
    # initialisation parametres
    n0 = X_train.shape[0]
    n2 = y_train.shape[0]
    np.random.seed(0)
    parametres = initialisation(n0, n1, n2)
    train_loss, test_loss = [], []
    train_acc, test_acc = [], []
    history = []
    # gradient descent
    for i in tqdm(range(n_iter)):
        activations_train = forward_propagation(X_train, parametres)
        A2_train = activations_train['A2']
        # Plot courbe d'apprentissage
        if i%10 == 0:
            train_loss.append(log_loss(y_train.flatten(), A2_train.flatten()))
            y_pred_train = predict(X_train, parametres)
            train_acc.append(accuracy_score(y_train.flatten(), y_pred_train.flatten()))

            activations_test = forward_propagation(X_test, parametres)
            A2_test = activations_test['A2']
            test_loss.append(log_loss(y_test.flatten(), A2_test.flatten()))
            y_pred_test = predict(X_test, parametres)
            test_acc.append(accuracy_score(y_test.flatten(), y_pred_test.flatten()))
        
            history.append([parametres.copy(), train_loss, train_acc, test_loss, test_acc, i])

        # mise a jour
        gradients = back_propagation(X_train, y_train, parametres, activations_train)
        parametres = update(gradients, parametres, learning_rate)


    plt.figure(figsize=(12, 4))
    plt.subplot(1, 2, 1)
    plt.plot(train_loss, label='train loss')
    plt.plot(test_loss, label='test loss')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(train_acc, label='train acc')
    plt.plot(test_acc, label='test acc')
    plt.legend()
    plt.show()

    return parametres









##### TEST DATA 1 #####
X, y = make_circles(n_samples=100, noise=0.1, factor=0.3, random_state=0)

# # 1 seul neurone : 
# y = y.reshape((y.shape[0], 1))
# plt.scatter(X[:, 0], X[:, 1], c=y, cmap='summer')
# plt.show()
# W, b = artificial_neuron(X, y)

# réseau de neurones
X = X.T
y = y.reshape((1, y.shape[0]))
print('dimensions de X:', X.shape)
print('dimensions de y:', y.shape)

parametres = neural_network(X, y, n1=2, n_iter=1000, learning_rate=0.1)





# ##### TEST DATA CATS AND DOGS #####
# X_train, y_train, X_test, y_test = load_data()

# X_train, X_test = X_train.T, X_test.T
# X_train = X_train.reshape(-1, X_train.shape[-1]) / X_train.max()
# X_test = X_test.reshape(-1, X_test.shape[-1]) / X_test.max()

# y_train, y_test = y_train.T, y_test.T

# m_train, m_test = 300, 80
# X_train, X_test = X_train[:, :m_train], X_test[:, :m_test]
# y_train, y_test = y_train[:, :m_train], y_test[:, :m_test]

# # print(X_train.shape)
# # print(X_test.shape)
# # print(y_train.shape)
# # print(y_test.shape)

# parametres = neural_network2(X_train, y_train, X_test, y_test, 
#             n1=32, n_iter=8000, learning_rate=0.01)
 
