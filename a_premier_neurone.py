import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.metrics import accuracy_score
import plotly.graph_objects as go


# _____ DATA _____
X, y = make_blobs(n_samples=100, n_features=2, centers=2, random_state=0)
y = y.reshape((y.shape[0], 1))
# print('dimension de X:', X.shape)
# print('dimension de y:', y.shape)
# plt.scatter(X[:,0], X[:,1], c=y, cmap='summer')
# plt.show()

# _____ INITIALISATION _____
def initialisation(X):
    W = np.random.randn(X.shape[1], 1)
    b = np.random.randn(1)
    return (W, b)
# (W, b) = initialisation(X)
# print(W.shape)

# _____ MODEL _____
def model(X, W, b):
    Z = X.dot(W) + b
    A = 1 / (1 + np.exp(-Z))
    return A
# A = model(X, W, b)
# print(A.shape)

# _____ COST _____
def log_loss(A, y):
    return 1 / len(y) * np.sum(-y * np.log(A) - (1 - y) * np.log(1 - A))
# print(log_loss(A, y))

# _____ GRADIENTS _____
def gradients(A, X, y):
    dW = 1 / len(y) * np.dot(X.T, A - y)
    db = 1 / len(y) * np.sum(A - y)
    return (dW, db)
# dW, db = gradients(A, X, y)
# print(dW.shape, db.shape)

# _____ UPDATE _____
def update(dW, db, W, b, learning_rate):
    W = W - learning_rate * dW
    b = b - learning_rate * db
    return (W, b)


# _____ PREDICTION _____
def predict(X, W, b):
    A = model(X, W, b)
    # print(A)
    return A >= 0.5






##### _____ ALGORITHM _____ #####
def artificial_neuron(X, y, learning_rate=0.1, n_iter=100):
    W, b = initialisation(X)
    Loss = []
    for i in range(n_iter):
        A = model(X, W, b)
        Loss.append(log_loss(A, y))
        dW, db = gradients(A, X, y)
        W, b = update(dW, db, W, b, learning_rate)
        plt.plot([min(X[:,0]), max(X[:,0])],
                [(W[0]*min(X[:,0])+b)*(-1/W[1]), (W[0]*max(X[:,0])+b)*(-1/W[1])],
                c='black', alpha=0.15)
    plt.scatter(X[:,0], X[:,1], c=y, cmap='summer')
    plt.xlim(min(X[:,0]), max(X[:,0]))
    plt.ylim(min(X[:,1]), max(X[:,1]))
    plt.show()
    y_pred = predict(X, W, b)
    print(accuracy_score(y, y_pred))
    plt.plot(Loss)
    plt.show()
    return (W, b)


if __name__ == "__main__":
    # 2D VISUALISATION + PREDICTION
    W, b = artificial_neuron(X, y)
    print("W", W, "\nb", b)
    new_plant1 = np.array([2, 1])
    new_plant2 = np.array([1, 5])
    plt.scatter(X[:,0], X[:,1], c=y, cmap='summer')
    plt.scatter(new_plant1[0], new_plant1[1], c='r', cmap='summer')
    plt.scatter(new_plant2[0], new_plant2[1], c='b', cmap='summer')
    plt.plot([min(X[:,0]), max(X[:,0])],
            [(W[0]*min(X[:,0])+b)*(-1/W[1]), (W[0]*max(X[:,0])+b)*(-1/W[1])],
            c='black', alpha=0.5)

    print("Prédiction 1 (rouge):", new_plant1, predict(new_plant1, W, b))
    print("Prédiction 2 (bleu):", new_plant2, predict(new_plant2, W, b))

    plt.show()



    # 3D VISUALISATION + PREDICTION
    # Data measured
    fig = go.Figure(data=[go.Scatter3d( 
        x=X[:, 0].flatten(),
        y=X[:, 1].flatten(),
        z=y.flatten(),
        mode='markers',
        marker=dict(
            size=5,
            color=y.flatten(),                
            colorscale='YlGn',  
            opacity=0.8,
            reversescale=True
        )
    )])
    fig.update_layout(template= "plotly_dark", margin=dict(l=0, r=0, b=0, t=0))
    fig.layout.scene.camera.projection.type = "orthographic"
    fig.show()


    X0 = np.linspace(X[:, 0].min(), X[:, 0].max(), 100)
    X1 = np.linspace(X[:, 1].min(), X[:, 1].max(), 100)
    xx0, xx1 = np.meshgrid(X0, X1)
    Z = W[0] * xx0 + W[1] * xx1 + b
    A = 1 / (1 + np.exp(-Z))

    fig = (go.Figure(data=[go.Surface(z=A, x=xx0, y=xx1, colorscale='YlGn', opacity = 0.7, reversescale=True)]))
    fig.add_scatter3d(x=X[:, 0].flatten(), y=X[:, 1].flatten(), z=y.flatten(), mode='markers', marker=dict(size=5, color=y.flatten(), colorscale='YlGn', opacity = 0.9, reversescale=True))

    fig.update_layout(template= "plotly_dark", margin=dict(l=0, r=0, b=0, t=0))
    fig.layout.scene.camera.projection.type = "orthographic"
    fig.show()