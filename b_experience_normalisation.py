from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np

from b_correction_exo_cat_dog import initialisation, model, gradients, update


# DATA de base
X, y = make_blobs(n_samples=100, n_features=2, centers=2, random_state=0)
X[:, 1] = X[:, 1] * 1
y = y.reshape(y.shape[0], 1)
plt.scatter(X[:,0], X[:, 1], c=y, cmap='summer')
plt.show()

def log_loss(A, y):
    epsilon = 1e-15
    return 1 / len(y) * np.sum(-y * np.log(A + epsilon) - (1 - y) * np.log(1 - A + epsilon))

def artificial_neuron_2(X, y, learning_rate=0.1, n_iter=1000):
  W, b = initialisation(X)
  W[0], W[1] = -7.5, 7.5
  nb, j = 1, 0
  history = np.zeros((n_iter // nb, 5))
  A = model(X, W, b)
  Loss, Params1, Params2 = [], [W[0]], [W[1]]
  Loss.append(log_loss(y, A))
  # Training
  for i in range(n_iter):
    A = model(X, W, b)
    Loss.append(log_loss(y, A))
    Params1.append(W[0])
    Params2.append(W[1])
    dW, db = gradients(A, X, y)
    W, b = update(dW, db, W, b, learning_rate = learning_rate)
    if (i % nb == 0):  
      history[j, 0] = W[0]
      history[j, 1] = W[1]
      history[j, 2] = b
      history[j, 3] = i
      history[j, 4] = log_loss(y, A)
      j +=1

  plt.plot(Loss)
  plt.show()

  return history, b, Loss, Params1, Params2





history, b, Loss, Params1, Params2 = artificial_neuron_2(X, y, learning_rate=0.6, n_iter=100)


lim = 15
h = 100
W1 = np.linspace(-lim, lim, h)
W2 = np.linspace(-lim, lim, h)
W11, W22 = np.meshgrid(W1, W2)
W_final = np.c_[W11.ravel(), W22.ravel()].T
print(W_final.shape)

Z = X.dot(W_final) + b
A = 1 / (1 + np.exp(-Z))
epsilon = 1e-15
L = 1 / len(y) * np.sum(-y * np.log(A + epsilon) - (1 - y) * np.log(1 - A + epsilon), axis=0).reshape(W11.shape)
print(L.shape)




# ######################
# # SI ON DILATE X1
# ######################
# coef = 5
# X2 = np.copy(X)
# X2[:, 1] = X2[:, 1] * coef
# history2, b2, Loss, Params1_2, Params2_2 = artificial_neuron_2(X2, y, learning_rate=0.6, n_iter=100)
# Z2 = X2.dot(W_final) + b2
# A2 = 1 / (1 + np.exp(-Z2))
# L2 = 1 / len(y) * np.sum(-y * np.log(A2 + epsilon) - (1 - y) * np.log(1 - A2 + epsilon), axis=0).reshape(W11.shape)


# plt.figure('Initial Data')
# plt.scatter(X[:,0], X[:, 1], c=y, cmap='summer')

# plt.figure('Coût initial')
# plt.contourf(W11, W22, L, cmap='magma')
# plt.colorbar()

# plt.figure('New Data')
# plt.scatter(X2[:,0], X2[:, 1], c=y, cmap='summer')

# plt.figure('Coût avec 1 direction dilatée')
# plt.contourf(W11, W22, L2, cmap='magma')
# plt.colorbar()
# plt.show()










#######################
# ANIMATION
#######################

from matplotlib.animation import FuncAnimation

def animate(params):
  W0 = params[0]
  W1 = params[1]
  b = params[2]
  i = params[3]
  loss = params[4]
  # ax[0].clear() # frontiere de décision
  # ax[1].clear() # sigmoide
  # ax[2].clear() # fonction Cout
  ax[0].contourf(W11, W22, L, 20, cmap='magma', zorder=-1)
  ax[0].scatter(Params1[int(i)], Params2[int(i)], c='r', marker='x', s=50, zorder=1)
  ax[0].plot(Params1[0:int(i)], Params2[0:int(i)], lw=3, c='r', zorder=1)
  ax[1].plot(Loss[0:int(i)], lw=3, c='white')
  ax[1].set_xlim(0, len(Params1))
  ax[1].set_ylim(min(Loss) - 2, max(Loss) + 2)
  
plt.figure(figsize=(12, 4))
plt.subplot(1, 2, 1)
plt.contourf(W11, W22, L, 10, cmap='magma')
plt.colorbar()
plt.subplot(1, 2, 2)
plt.contourf(W11, W22, L, 10, cmap='magma')
plt.scatter(history[:, 0], history[:, 1], c=history[:, 2], cmap='Blues', marker='x')
plt.plot(history[:, 0], history[:, 1])
# plt.plot(history[:, 0], history[:, 1], c='r')
# plt.colorbar()
fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(40, 10))
ani = FuncAnimation(fig, animate, frames=history, interval=10, repeat=False)

import matplotlib.animation as animation

Writer = animation.writers['ffmpeg']
writer = Writer(fps=10, metadata=dict(artist='Me'), bitrate=3200)
ani.save('animation3.mp4', writer=writer)


# import plotly.graph_objects as go

# fig = (go.Figure(data=[go.Surface(z=L, x=W11, y=W22, opacity = 1)]))

# fig.update_layout(template= "plotly_dark", margin=dict(l=0, r=0, b=0, t=0))
# fig.layout.scene.camera.projection.type = "orthographic"
# fig.show()

